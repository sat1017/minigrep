use std::env;
use std::process;

use minigrep::Config;


fn main() {
    /*
    Regex:
    -i      ignore case distinctions
    Output control:
    -r      recursively search directory
    -f      search a file
    -c      print only a count of matching lines per files
    -n      print line number with output lines
    -h      print the file name for each match

    When feasible, the Boyer–Moore fast string searching algorithm is used to match a 
    single fixed pattern,and the Aho–Corasick algorithm is used to match multiple 
    fixed patterns.

    cargo run -- -i to ./books/iliad.txt
    
     */
    let config = Config::build(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {err}");
        process::exit(1);
    });

    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {e}");
        process::exit(1);
    };

    // dbg!(args);
}


